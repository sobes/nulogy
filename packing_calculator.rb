class PackingCalculator

  MARKUPS = {
    flat: 0.05, 
    per_person: 0.012, 
    categories: {
      "drugs" => 0.075, 
      "food" => 0.13, 
      "electronics" => 0.02
    }
  }

  attr_reader :initial_base_price, :person_count, :category

  def initialize(initial_base_price, person_count, category)

    @initial_base_price = initial_base_price
    @person_count = person_count
    @category = category

  end

  def calculate_price

    with_flat_markup = @initial_base_price * (1 + MARKUPS[:flat])
    with_person_markup = with_flat_markup * (1 + MARKUPS[:per_person] * @person_count)

    if MARKUPS[:categories][@category]
      return (with_person_markup + with_flat_markup * MARKUPS[:categories][@category]).round(2)
    else
      return with_person_markup.round(2)
    end

  end

end
