require_relative 'packing_calculator'

describe PackingCalculator, "#calculate_price" do


  MARKUPS = PackingCalculator::MARKUPS

  let(:person_count)             { rand(1..10) }
  let(:initial_base_price)       { (rand(100000..2000000).to_f/100).round(2) }
  let(:price_with_flat_markup)   { initial_base_price * (1 + MARKUPS[:flat]) }
  let(:price_with_person_markup) { price_with_flat_markup * (1 + MARKUPS[:per_person] * person_count) }


  it "returns an additional #{MARKUPS[:categories]["drugs"]} markup on drugs" do

    pc = PackingCalculator.new(initial_base_price, person_count, "drugs")
    expected_result = (price_with_person_markup + price_with_flat_markup * MARKUPS[:categories]["drugs"]).round(2)

    pc.calculate_price.should == expected_result

  end

  it "returns an additional #{MARKUPS[:categories]["food"]} markup on food" do

    pc = PackingCalculator.new(initial_base_price, person_count, "food")
    expected_result = (price_with_person_markup + price_with_flat_markup * MARKUPS[:categories]["food"]).round(2)

    pc.calculate_price.should == expected_result

  end

  it "returns an additional #{MARKUPS[:categories]["electronics"]} markup on electronics" do

    pc = PackingCalculator.new(initial_base_price, person_count, "electronics")
    expected_result = (price_with_person_markup + price_with_flat_markup * MARKUPS[:categories]["electronics"]).round(2)

    pc.calculate_price.should == expected_result

  end

  it "returns no additonal markup on a category not in (drugs, food, electronics)" do

    pc = PackingCalculator.new(initial_base_price, person_count, "books")
    expected_result = price_with_person_markup.round(2)

    pc.calculate_price.should == expected_result

  end

end
